"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

export class BallCannon extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "BallCannon", active: true });
            this.gameOn=true;
            this.score=0;
            this.scoreMsg="Score: ";
            this.scoreText;
            this.angel=null;
            this.angelMsg="Angel: ";
            this.angelText=null;
            this.power=null;
            this.powerMsg="Power: ";
            this.powerText=null;
            this.my_buttons = [];
            this.step=4;
            this.inputTime=2000;
            this.firstValue=true;
            this.secoundValue=true;
        }
        
        preload() {
			this.load.spritesheet('mushrooms', assetsPath+'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
		}

        create ()
        {
		    this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
			
	    this.angel = this.tweens.addCounter({
        from: 0,
        to: 90,
        duration: this.inputTime,
        yoyo: true,
        repeat: -1
        });
        
        this.angelText = this.add.text(30, 20, this.angelMsg+this.angel.getValue(), { font: '22px Courier', fill: '#FF0000' });
        
        this.power = this.tweens.addCounter({
        from: 0,
        to: 100,
        duration: this.inputTime,
        yoyo: true,
        paused: true,
        repeat: -1
        });
        
        this.powerText = this.add.text(30, 40, this.powerMsg+this.power.getValue(), { font: '22px Courier', fill: '#FF0000' });
        
        // Create a bullet
	    this.pen = this.make.graphics({x: 0, y: 0, add: false});
        this.pen.fillStyle(0x000000, 1.0);
        this.pen.fillCircle(10, 10, 10, 10);
        this.pen.generateTexture('bullet', 20, 20);
        
        // Create a mushroom 
        this.mushroom = this.physics.add.staticImage(400, 300, 'mushrooms',0);
        }

        update()
        {
		  if(this.gameOn) {
			  this.angelText.setText(this.angelMsg+Math.floor(this.angel.getValue()));
			  this.powerText.setText(this.powerMsg+Math.floor(this.power.getValue()));
			  if(Phaser.Input.Keyboard.JustDown(this.spacebar)) {
				  if(this.firstValue) {
				      this.angel.pause();
				      this.power.play();
				      this.firstValue=false;
			      }
			      else if(this.secoundValue) {
					  this.secoundValue=false;
					  this.power.pause();
					  this.shoot();
					  
				  }
			  }
		}
  
       }
       
       shoot()
        {
			console.log("Shoot");
            let bullet = this.physics.add.image(50, 600, 'bullet');
            bullet.body.velocity.x = Math.cos(this.angel.getValue()/180*Math.PI)*this.power.getValue()*5;
            bullet.body.velocity.y = -Math.sin(this.angel.getValue()/180*Math.PI)*this.power.getValue()*5;
            this.physics.add.overlap(bullet, this.mushroom, this.hit); 
	    }
	    
	    hit(bullet, mushroom)
        {
            console.log("hit");
            bullet.destroy();
            mushroom.destroy();
	    }
   }

